import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';
import {Family} from "../../models";

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.initForm();
  }
  private initForm(){
    this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(30)]),
      father: new FormGroup({
        name: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(30)]),
        age: new FormControl(null, [Validators.required, Validators.pattern(/^([1-9]\d*)?$/)])
      }),
      mother: new FormGroup({
        name: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(30)]),
        age: new FormControl(null, [Validators.required, Validators.pattern(/^([1-9]\d*)?$/)])
      }),
      children: new FormArray([
        new FormGroup({
          name: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(30)]),
          age: new FormControl(null, [Validators.required, Validators.pattern(/^([1-9]\d*)?$/)])
        })
      ])
    });
  }
  public addChild() {
    this.children.push(new FormGroup({
        name: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(30)]),
        age: new FormControl(null, [Validators.required, Validators.pattern(/^([1-9]\d*)?$/)])
      })
    );
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    const newFamily: Family = this.familyForm.value;
    this.familyService.addFamily$(newFamily).subscribe();
    this.familyForm.reset();
  }
}
